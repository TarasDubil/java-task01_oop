package com.dubilok.view;

import com.dubilok.model.manager.Manager;
import com.dubilok.model.plane.Plane;
import com.dubilok.view.menu.Menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ConsoleView implements View {

    @Override
    public final void show(final Manager manager) {
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(System.in))) {
            while (true) {
                System.out.println("if you want to sort Planes by "
                        + "Flight Range enter: '1'\n if you want to"
                        + "find a all planes by fuel enter: '2'\n"
                        + "if you want to see total passenger capacity"
                        + " enter: '3'\nif you want to see total carrying"
                        + " capacity enter: '4'\n"
                        + "if you want to exit enter: '0'");
                String message = bufferedReader.readLine();
                if (message.equals(Menu.SORT.getMessage())) {
                    List<Plane> planes = manager.getPlanePark()
                            .sortByFlightRange(manager
                                    .getCompany().getPlanes());
                    printAll(planes);
                } else if (message.equals(Menu.FIND.getMessage())) {
                    System.out.println("Enter your fuel!");
                    int fuel = Integer.parseInt(bufferedReader.readLine());
                    List<Plane> planes = manager.getPlanePark()
                            .getPlaneByFuel(manager
                                    .getCompany().getPlanes(), fuel);
                    printAll(planes);
                } else if (message.equals(Menu.PASSENGER.getMessage())) {
                    int passengerCapacity = manager.getPlanePark().
                            getTotalPassengerCapacity(manager
                                    .getCompany().getPlanes());
                    System.out.println("Total passenger capacity = "
                            + passengerCapacity);
                } else if (message.equals(Menu.CARRYING.getMessage())) {
                    double carryingCapacity = manager.getPlanePark()
                            .getTotalCarryingCapacity(manager
                                    .getCompany().getPlanes());
                    System.out.println("Total carrying capacity = "
                            + carryingCapacity);
                } else if (message.equals(Menu.EXIT.getMessage())) {
                    System.out.println("End!");
                    break;
                } else {
                    System.out.println("Repeat your choise!");
                }
                System.out.println("\n----------------------------"
                        + "-----------------------------------");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printAll(final List<Plane> list) {
        list.forEach(System.out::println);
    }
}
