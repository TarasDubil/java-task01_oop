package com.dubilok.view;

import com.dubilok.model.manager.Manager;

public interface View {
    void show(Manager manager);
}
