package com.dubilok.view.menu;

public enum Menu {
    SORT("1"),
    FIND("2"),
    PASSENGER("3"),
    CARRYING("4"),
    EXIT("0");

    private String message;

    Menu(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
