package com.dubilok.model.planepark;

import com.dubilok.model.plane.Plane;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PlaneParkImpl implements PlanePark {
    public final List<Plane> sortByFlightRange(final List<Plane> planes) {
        return planes.stream().sorted(Comparator
                .comparing(Plane::getFlightRange)).collect(Collectors.toList());
    }

    public final List<Plane> getPlaneByFuel(final List<Plane> planes,
                                            final double fuel) {
        List<Plane> list = new ArrayList<>();
        for (Plane p : planes) {
            if (p.getFuelConsumption() <= fuel) {
                list.add(p);
            }
        }
        return list;
    }

    public final int getTotalPassengerCapacity(final List<Plane> planes) {
        int totalPassengerCapacity = 0;
        for (Plane p : planes) {
            totalPassengerCapacity += p.getPassengerCapacity();
        }
        return totalPassengerCapacity;
    }

    public final double getTotalCarryingCapacity(final List<Plane> planes) {
        int totalCarryingCapacity = 0;
        for (Plane p : planes) {
            totalCarryingCapacity += p.getCarryingCapacity();
        }
        return totalCarryingCapacity;
    }
}
