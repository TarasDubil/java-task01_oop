package com.dubilok.model.planepark;

import com.dubilok.model.plane.Plane;

import java.util.List;

public interface PlanePark {
    List<Plane> sortByFlightRange(List<Plane> planes);

    List<Plane> getPlaneByFuel(List<Plane> planes, double fuel);

    int getTotalPassengerCapacity(List<Plane> planes);

    double getTotalCarryingCapacity(List<Plane> planes);
}
