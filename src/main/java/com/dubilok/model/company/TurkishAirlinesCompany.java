package com.dubilok.model.company;

import com.dubilok.model.plane.Boeing737;
import com.dubilok.model.plane.Boeing747;
import com.dubilok.model.plane.Cessna150;
import com.dubilok.model.plane.Plane;

import java.util.ArrayList;
import java.util.List;

public final class TurkishAirlinesCompany implements Company {

    private static TurkishAirlinesCompany instance;
    private List<Plane> planes = new ArrayList<>();

    private TurkishAirlinesCompany() {
    }

    {
        planes.add(new Boeing737("Boeing 737", 300, 3000, 2000, 40));
        planes.add(new Boeing747("Boeing 747", 500, 4000, 3000, 50));
        planes.add(new Cessna150("Cessna 150", 30, 500, 300, 20));
    }

    public static TurkishAirlinesCompany getInstance() {
        if (instance == null) {
            instance = new TurkishAirlinesCompany();
        }
        return instance;
    }

    public List<Plane> getPlanes() {
        return planes;
    }

    @Override
    public String toString() {
        return "TurkishAirlinesCompany{"
                + "planes=" + planes
                + '}';
    }
}
