package com.dubilok.model.company;

import com.dubilok.model.plane.Plane;

import java.util.List;

public interface Company {
    List<Plane> getPlanes();
}
