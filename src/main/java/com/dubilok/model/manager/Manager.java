package com.dubilok.model.manager;

import com.dubilok.model.company.Company;
import com.dubilok.model.planepark.PlanePark;

public class Manager {

    private final Company company;
    private final PlanePark planePark;

    public Manager(final Company company, final PlanePark planePark) {
        this.company = company;
        this.planePark = planePark;
    }

    public final Company getCompany() {
        return company;
    }

    public final PlanePark getPlanePark() {
        return planePark;
    }
}
