package com.dubilok.model.plane;

public abstract class RegionalPlane extends Plane {
    public RegionalPlane(final String title, final int passengerCapacity,
                         final double carryingCapacity,
                         final double flightRange,
                         final double fuelConsumption) {
        super(title, passengerCapacity, carryingCapacity,
                flightRange, fuelConsumption);
    }
}
