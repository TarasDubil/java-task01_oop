package com.dubilok.model.plane;

public class Boeing737 extends NarrowBodyPlane {
    public Boeing737(final String title, final int passengerCapacity,
                     final double carryingCapacity, final double flightRange,
                     final double fuelConsumption) {
        super(title, passengerCapacity, carryingCapacity,
                flightRange, fuelConsumption);
    }
}
