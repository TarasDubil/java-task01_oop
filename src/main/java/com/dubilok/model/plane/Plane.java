package com.dubilok.model.plane;

public class Plane {
    private String title;
    private int passengerCapacity;
    private double carryingCapacity;
    private double flightRange;
    private double fuelConsumption;

    public Plane(final String title, final int passengerCapacity,
                 final double carryingCapacity, final double flightRange,
                 final double fuelConsumption) {
        this.title = title;
        this.passengerCapacity = passengerCapacity;
        this.carryingCapacity = carryingCapacity;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
    }

    public final String getTitle() {
        return title;
    }

    public final int getPassengerCapacity() {
        return passengerCapacity;
    }

    public final double getCarryingCapacity() {
        return carryingCapacity;
    }

    public final double getFlightRange() {
        return flightRange;
    }

    public final double getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public final String toString() {
        return "Plane{"
                + "title='" + title + '\''
                + ", passengerCapacity=" + passengerCapacity
                + ", carryingCapacity=" + carryingCapacity
                + ", flightRange=" + flightRange
                + ", fuelConsumption=" + fuelConsumption
                + '}';
    }
}
