package com.dubilok.model.plane;

public class Cessna150 extends LocalPlane {
    public Cessna150(final String title, final int passengerCapacity,
                     final double carryingCapacity, final double flightRange,
                     final double fuelConsumption) {
        super(title, passengerCapacity, carryingCapacity,
                flightRange, fuelConsumption);
    }
}
