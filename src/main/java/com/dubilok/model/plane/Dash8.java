package com.dubilok.model.plane;

public class Dash8 extends RegionalPlane {
    public Dash8(final String title, final int passengerCapacity,
                 final double carryingCapacity, final double flightRange,
                 final double fuelConsumption) {
        super(title, passengerCapacity, carryingCapacity,
                flightRange, fuelConsumption);
    }
}
