package com.dubilok.model.plane;

public abstract class WideBodyPlane extends Plane {
    public WideBodyPlane(final String title, final int passengerCapacity,
                         final double carryingCapacity,
                         final double flightRange,
                         final double fuelConsumption) {
        super(title, passengerCapacity, carryingCapacity,
                flightRange, fuelConsumption);
    }
}
