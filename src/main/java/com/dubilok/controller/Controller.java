package com.dubilok.controller;

import com.dubilok.model.company.TurkishAirlinesCompany;
import com.dubilok.model.manager.Manager;
import com.dubilok.model.planepark.PlaneParkImpl;
import com.dubilok.view.ConsoleView;
import com.dubilok.view.View;

public final class Controller {

    private Controller() {
    }

    public static void main(final String[] args) {
        Manager manager = new Manager(TurkishAirlinesCompany.getInstance(),
                new PlaneParkImpl());
        View view = new ConsoleView();
        view.show(manager);
    }

}
